package crudxml;

import com.bumptech.glide.Glide;
import com.dharma.crudxml.R;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class HalamanUtamaActivity extends Activity {

		//buat 2 variabel untuk objek button
		Button btnLihatAnggota;
	    Button btnTambahAnggota;
	 
	    @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.halaman_utama);

			ImageView image = (ImageView) findViewById(R.id.imageView);

            new LoadImagefromUrl( ).execute( image, "http://178.128.83.56/crudxmlphp/images/8.png");
	 
	        // definisikan 2 buah button 
	        btnLihatAnggota = (Button) findViewById(R.id.btnLihatAnggota);
	        btnTambahAnggota = (Button) findViewById(R.id.btnTambahAnggota);
	 
	        // Even klik untuk melihat data anggota
	        btnLihatAnggota.setOnClickListener(new View.OnClickListener() {
	 
	            @Override
	            public void onClick(View view) {
	                //Tampilkan halaman semua anggota activity
	                Intent i = new Intent(getApplicationContext(), SemuaAnggotaActivity.class);
	                startActivity(i);
	                finish();
	            }
	        });
	 
	        // even klik tambah data anggota
	        btnTambahAnggota.setOnClickListener(new View.OnClickListener() {
	 
	            @Override
	            public void onClick(View view) {
	               //Tampilkan halaman tambah anggota activity
	               Intent i = new Intent(getApplicationContext(), TambahAnggotaActivity.class);
	                startActivity(i);
	                finish();
	            }
	        });
	    }

    private class LoadImagefromUrl extends AsyncTask< Object, Void, Bitmap > {
        ImageView ivPreview = null;

        @Override
        protected Bitmap doInBackground( Object... params ) {
            this.ivPreview = (ImageView) params[0];
            String url = (String) params[1];
            System.out.println(url);
            return loadBitmap( url );
        }

        @Override
        protected void onPostExecute( Bitmap result ) {
            super.onPostExecute( result );
            ivPreview.setImageBitmap( result );
        }
    }

    public Bitmap loadBitmap( String url ) {
        URL newurl = null;
        Bitmap bitmap = null;
        try {
            newurl = new URL( url );
            bitmap = BitmapFactory.decodeStream( newurl.openConnection( ).getInputStream( ) );
        } catch ( MalformedURLException e ) {
            e.printStackTrace( );
        } catch ( IOException e ) {

            e.printStackTrace( );
        }
        return bitmap;
    }
	}

