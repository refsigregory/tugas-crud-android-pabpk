package crudxml;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.net.URL;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.bumptech.glide.Glide;
import com.dharma.crudxml.R;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import static android.content.ContentValues.TAG;

public class SemuaAnggotaActivity extends ListActivity {

	// variabel statis url xml(sesuaikan letak path file member.xml)
	static final String URL = "http://178.128.83.56/crudxmlphp/member.xml";
	// XML node keys
	static final String KEY_MEMBER = "member"; // parent node
	static final String KEY_ID = "id";
	static final String KEY_NAMA = "nama";
	static final String KEY_ALAMAT = "alamat";

    static final String KEY_GENDER = "gender";
    static final String KEY_AGAMA = "agama";
    static final String KEY_TANGGAL = "tanggal";
    static final String KEY_BAHASA = "bahasa";
    static final String KEY_GAMBAR= "gambar";
	ArrayList<HashMap<String, String>> menuItems = new ArrayList<HashMap<String, String>>();
	 

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.semua_anggota);

        //new LoadImagefromUrl( ).execute( image, "http://178.128.83.56/crudxmlphp/images/8.png");

		// Buat AmbilData pada background thread
		 new AmbilDataXML().execute();
		 // Ambil listview
	        ListView lv = getListView();
	 
	        // Ketika memilih salah satu anggota
	        // Tampilkan edit data anggota
	        lv.setOnItemClickListener(new OnItemClickListener() {
	 
	            @Override
	            public void onItemClick(AdapterView<?> parent, View view,
	                    int position, long id) {
	                // getting values from selected ListItem
	                String idmem = ((TextView) view.findViewById(R.id.idmem)).getText().toString();
	                String nama = ((TextView) view.findViewById(R.id.nama)).getText().toString();
	                String alamat = ((TextView) view.findViewById(R.id.alamat)).getText().toString();
					String gambar = ((TextView) view.findViewById(R.id.gambar)).getText().toString();
					String agama = ((TextView) view.findViewById(R.id.agama)).getText().toString();
					String bahasa = ((TextView) view.findViewById(R.id.bahasa)).getText().toString();
					String gender = ((TextView) view.findViewById(R.id.gender)).getText().toString();
					String tanggal = ((TextView) view.findViewById(R.id.tanggal)).getText().toString();
	 
	                // Mulai intent baru ke tampilan editanggotaactivity
	                Intent in = new Intent(getApplicationContext(),EditAnggotaActivity.class);
	                // kirim 3 variabel nilai ke activity berikutnya
	                in.putExtra(KEY_ID, idmem);
	                in.putExtra(KEY_NAMA, nama);
	                in.putExtra(KEY_ALAMAT, alamat);
					in.putExtra(KEY_GAMBAR, gambar);
					in.putExtra(KEY_AGAMA, agama);
					in.putExtra(KEY_TANGGAL, tanggal);
					in.putExtra(KEY_GENDER, gender);
					in.putExtra(KEY_BAHASA, bahasa);

	                startActivity(in);
	 
	            }
	        });




	}


	private class LoadImagefromUrl extends AsyncTask< Object, Void, Bitmap > {
		ImageView ivPreview = null;

		@Override
		protected Bitmap doInBackground( Object... params ) {
			this.ivPreview = (ImageView) params[0];
			String url = (String) params[1];
			System.out.println(url);
			return loadBitmap( url );
		}

		@Override
		protected void onPostExecute( Bitmap result ) {
			super.onPostExecute( result );
			ivPreview.setImageBitmap( result );
		}
	}

	public Bitmap loadBitmap( String url ) {
		URL newurl = null;
		Bitmap bitmap = null;
		try {
			newurl = new URL( url );
			bitmap = BitmapFactory.decodeStream( newurl.openConnection( ).getInputStream( ) );
		} catch ( MalformedURLException e ) {
			e.printStackTrace( );
		} catch ( IOException e ) {

			e.printStackTrace( );
		}
		return bitmap;
	}
	
	/**
     * Background Async Task untuk mengambil dan menampilkan data anggota
     * */
    class AmbilDataXML extends AsyncTask<String, String, String> {
        private ProgressDialog pDialog;
		@Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(SemuaAnggotaActivity.this);
            pDialog.setMessage("Loading Data Dulu...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }
	
        protected String doInBackground(String... args) {
		XMLParser parser = new XMLParser();
		String xml = parser.getXmlFromUrl(URL); // ambil XML
		Document doc = parser.getDomElement(xml); // ambil DOM elemen

		NodeList nl = doc.getElementsByTagName(KEY_MEMBER);
		// looping semua item nodes <item>
		for (int i = 0; i < nl.getLength(); i++) {
			// Buat new HashMap
            HashMap<String, String> map = new HashMap<String, String>();
            Element e = (Element) nl.item(i);
            // tambahkan child node ke HashMap key => value
            map.put(KEY_ID, parser.ambilNilai(e, KEY_ID));
            map.put(KEY_NAMA, parser.ambilNilai(e, KEY_NAMA));
            map.put(KEY_ALAMAT, parser.ambilNilai(e, KEY_ALAMAT));
            map.put(KEY_GAMBAR, parser.ambilNilai(e, KEY_GAMBAR));
            map.put(KEY_AGAMA, parser.ambilNilai(e, KEY_AGAMA));
            map.put(KEY_TANGGAL, parser.ambilNilai(e, KEY_TANGGAL));
            map.put(KEY_BAHASA, parser.ambilNilai(e, KEY_BAHASA));
            map.put(KEY_GENDER, parser.ambilNilai(e, KEY_GENDER));
 
            // Tambah HashList ke ArrayList
            menuItems.add(map);
		}
        return null;
		
        }
        protected void onPostExecute(String dataXML) {
            pDialog.dismiss();
        	runOnUiThread(new Runnable() {
                public void run() {
                	ListAdapter adapter = new SimpleAdapter(
							SemuaAnggotaActivity.this, menuItems,
							R.layout.list_item, new String[] {KEY_ID, KEY_NAMA,KEY_ALAMAT,KEY_GAMBAR,KEY_AGAMA,KEY_TANGGAL,KEY_BAHASA,KEY_GENDER}, new int[] {R.id.idmem, R.id.nama, R.id.alamat,R.id.gambar,R.id.agama,R.id.tanggal,R.id.bahasa,R.id.gender});
                    setListAdapter(adapter);
                }
            });

        }
    }
}