package crudxml;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import com.dharma.crudxml.R;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.AttrRes;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.content.Context;

import javax.net.ssl.HttpsURLConnection;

import static com.dharma.crudxml.R.id.gender2;

public class EditAnggotaActivity  extends Activity {

    EditText txtNama;
    EditText txtAlamat;
    Button btnSave;
    Button btnDelete;
    private ProgressDialog pDialog;
    String idmem;
    String nama;
    String alamat;
    boolean uploadGambar = false;

    String FinalData;


    private DatePickerDialog datePickerDialog;
    private SimpleDateFormat dateFormatter;
    private TextView tvDateResult;
    private Button btDatePicker;

    EditText inputNama;
    EditText inputAlamat;
    RadioGroup radioGroup;
    RadioButton gender;
    RadioButton gender2;
    CheckBox id,en,kr,jp;

    String jk,bahasa="",tanggal,agama,gambar;
    private Spinner spinner1;


    // upload image

    Bitmap bitmap;

    boolean check = true;

    Button SelectImageGallery, UploadImageServer;

    ImageView imageView;

    EditText imageName;

    ProgressDialog progressDialog ;

    String GetImageNameEditText;

    String ImageName = "image_name" ;

    String ImagePath = "image_path" ;

    String ServerUploadPath ="http://178.128.83.56/crudxmlphp/img_upload_to_server.php";

    //--

    // XML node keys
    static final String KEY_ID = "id";
    static final String KEY_NAMA = "nama";
    static final String KEY_ALAMAT = "alamat";
    static final String KEY_GENDER = "gender";
    static final String KEY_AGAMA = "agama";
    static final String KEY_TANGGAL = "tanggal";
    static final String KEY_BAHASA = "bahasa";
    static final String KEY_GAMBAR= "gambar";

    ArrayAdapter<CharSequence> adapter;

     String compareValue = "Pilih Agama";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_anggota);



        spinner1 = (Spinner) findViewById(R.id.spinner);
        spinner1.setOnItemSelectedListener(new CustomOnItemSelectedListener());
        adapter = ArrayAdapter.createFromResource(this, R.array.android_layout_arrays, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        gender = (RadioButton) findViewById(R.id.gender);
        gender2 =  (RadioButton) findViewById(R.id.gender2);

        // save button
        btnSave = (Button) findViewById(R.id.btnSave);
        btnDelete = (Button) findViewById(R.id.btnDelete);



        // buat method untuk Mengambil data detail anggota pada background thread
        new AmbilDetailAnggota().execute();

        // event klik pada button save/update
        btnSave.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // mulai background task to menyimpan/update data anggota
                new SimpanAnggotaDetail().execute();
            }
        });

        // Even klik pada Delete button
        btnDelete.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // mulai menghapus data anggota pada background task
                new HapusAnggota().execute();
            }
        });



        // definisikan Edit Text
        inputNama = (EditText) findViewById(R.id.inputNama);
        inputAlamat = (EditText) findViewById(R.id.inputAlamat);
        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        id = (CheckBox) findViewById(R.id.checkBox);
        en = (CheckBox) findViewById(R.id.checkBox2);
        kr = (CheckBox) findViewById(R.id.checkBox3);
        jp = (CheckBox) findViewById(R.id.checkBox4);


/**
 * Kita menggunakan format tanggal dd-MM-yyyy
 * jadi nanti tanggal nya akan diformat menjadi
 * misalnya 01-12-2017
 */
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

        tvDateResult = (TextView) findViewById(R.id.tv_dateresult);
        btDatePicker = (Button) findViewById(R.id.bt_datepicker);
        btDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDateDialog();
            }
        });

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                RadioButton rb=(RadioButton)findViewById(checkedId);
                //setText("Kamu Memilih" + rb.getText());
                jk = (String) rb.getText();
                Toast.makeText(getApplicationContext(), rb.getText(), Toast.LENGTH_SHORT).show();

            }
        });


        imageView = (ImageView)findViewById(R.id.imageView);

        imageName = (EditText)findViewById(R.id.editTextImageName);

        SelectImageGallery = (Button)findViewById(R.id.buttonSelect);

        UploadImageServer = (Button)findViewById(R.id.buttonUpload);

        SelectImageGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent();

                intent.setType("image/*");

                intent.setAction(Intent.ACTION_GET_CONTENT);

                startActivityForResult(Intent.createChooser(intent, "Select Image From Gallery"), 1);

            }
        });


        UploadImageServer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                GetImageNameEditText = imageName.getText().toString();

                    ImageUploadToServerFunction();


            }
        });
    }


    @Override
    protected void onActivityResult(int RC, int RQC, Intent I) {

        super.onActivityResult(RC, RQC, I);

        if (RC == 1 && RQC == RESULT_OK && I != null && I.getData() != null) {

            Uri uri = I.getData();


            uploadGambar = true;

            try {

                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);

                imageView.setImageBitmap(bitmap);


            } catch (IOException e) {

                e.printStackTrace();
            }
        }
    }

    private class LoadImagefromUrl extends AsyncTask< Object, Void, Bitmap > {
        ImageView ivPreview = null;

        @Override
        protected Bitmap doInBackground( Object... params ) {
            this.ivPreview = (ImageView) params[0];
            String url = (String) params[1];
            System.out.println(url);
            return loadBitmap( url );
        }

        @Override
        protected void onPostExecute( Bitmap result ) {
            super.onPostExecute( result );
            ivPreview.setImageBitmap( result );
        }
    }

    public Bitmap loadBitmap( String url ) {
        URL newurl = null;
        Bitmap bitmap = null;
        try {
            newurl = new URL( url );
            bitmap = BitmapFactory.decodeStream( newurl.openConnection( ).getInputStream( ) );
        } catch ( MalformedURLException e ) {
            e.printStackTrace( );
        } catch ( IOException e ) {

            e.printStackTrace( );
        }
        return bitmap;
    }

    public void ImageUploadToServerFunction(){

        ByteArrayOutputStream byteArrayOutputStreamObject ;

        byteArrayOutputStreamObject = new ByteArrayOutputStream();
        if(uploadGambar) {
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStreamObject);
        }

        byte[] byteArrayVar = byteArrayOutputStreamObject.toByteArray();

        final String ConvertImage = Base64.encodeToString(byteArrayVar, Base64.DEFAULT);

        class AsyncTaskUploadClass extends AsyncTask<Void,Void,String> {

            @Override
            protected void onPreExecute() {

                super.onPreExecute();

                progressDialog = ProgressDialog.show(EditAnggotaActivity.this,"Mengubah Data Angggota","Please Wait",false,false);
            }

            @Override
            protected void onPostExecute(String string1) {

                super.onPostExecute(string1);

                // Dismiss the progress dialog after done uploading.
                progressDialog.dismiss();

                // Printing uploading success message coming from server on android app.
                Toast.makeText(EditAnggotaActivity.this,string1,Toast.LENGTH_LONG).show();

                // Setting image as transparent after done uploading.
                imageView.setImageResource(android.R.color.transparent);


            }

            @Override
            protected String doInBackground(Void... params) {
                if(uploadGambar) {

                    EditAnggotaActivity.ImageProcessClass imageProcessClass = new EditAnggotaActivity.ImageProcessClass();

                    HashMap<String, String> HashMapParams = new HashMap<String, String>();

                    HashMapParams.put(ImageName, GetImageNameEditText);

                    HashMapParams.put(ImagePath, ConvertImage);



                    FinalData = imageProcessClass.ImageHttpRequest(ServerUploadPath, HashMapParams);
                }

                // --


                String nama = inputNama.getText().toString();
                String alamat = inputAlamat.getText().toString();


                Intent i = new Intent(getApplicationContext(), SemuaAnggotaActivity.class);
                // tutup semua activity sebelumnya
                startActivity(i);
                finish();

                //--

                //ambil intent data id anggota

                //jika selesai menyimpan/update kembali ke activity semuanggotaactivity
                //Intent in = new Intent(getApplicationContext(), SemuaAnggotaActivity.class);
                Intent in = getIntent();
                //ambil XML values dari intent sebelumnya
                idmem = in.getStringExtra(KEY_ID);

                // gambil data dari EditTexts
                nama = txtNama.getText().toString();
                alamat = txtAlamat.getText().toString();


                try {
                    if(id.isChecked()) {
                        bahasa = "Bahasa Indonesia,";
                    }

                    if(en.isChecked()) {
                        bahasa = bahasa + "Bahasa Inggris,";
                    }

                    if(kr.isChecked()) {
                        bahasa = bahasa + "Bahasa Korea,";
                    }

                    if(jp.isChecked()) {
                        bahasa = bahasa + "Bahasa Jepang,";
                    }

                    DefaultHttpClient client = new DefaultHttpClient();
                    String postURL = "http://178.128.83.56/crudxmlphp/updateanggota.php";
                    HttpPost post = new HttpPost(postURL);
                    // buat Parameter
                    List<NameValuePair> param= new ArrayList<NameValuePair>();
                    param.add(new BasicNameValuePair("idmem", idmem));
                    param.add(new BasicNameValuePair("nama", nama));
                    param.add(new BasicNameValuePair("alamat", alamat));

                    param.add(new BasicNameValuePair("gender", jk));
                    param.add(new BasicNameValuePair("bahasa", bahasa));
                    param.add(new BasicNameValuePair("tanggal", tanggal));
                    param.add(new BasicNameValuePair("agama", agama));

                    if(!uploadGambar) {
                        param.add(new BasicNameValuePair("gambar", gambar));

                    }

                    System.out.println(param);
                    UrlEncodedFormEntity ent = new UrlEncodedFormEntity(param,HTTP.UTF_8);
                    post.setEntity(ent);
                    HttpResponse responsePOST = client.execute(post);
                    HttpEntity resEntity = responsePOST.getEntity();
                    if (resEntity != null) {
                        Log.i("RESPONSE",EntityUtils.toString(resEntity));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                // tutup semua activity sebelumnya
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);


                if(uploadGambar){
                    return FinalData;
                }else {
                    return null;
                }

            }
        }
        AsyncTaskUploadClass AsyncTaskUploadClassOBJ = new AsyncTaskUploadClass();

        AsyncTaskUploadClassOBJ.execute();
    }





    private void showDateDialog()
    {

        /**
         * Calendar untuk mendapatkan tanggal sekarang
         */
        Calendar newCalendar = Calendar.getInstance();

        /**
         * Initiate DatePicker dialog
         */
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                /**
                 * Method ini dipanggil saat kita selesai memilih tanggal di DatePicker
                 */

                /**
                 * Set Calendar untuk menampung tanggal yang dipilih
                 */
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);

                /**
                 * Update TextView dengan tanggal yang kita pilih
                 */
                tvDateResult.setText("Tanggal dipilih : "+dateFormatter.format(newDate.getTime()));
                tanggal = dateFormatter.format(newDate.getTime());
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        /**
         * Tampilkan DatePicker dialog
         */
        datePickerDialog.show();
    }

    public class ImageProcessClass{

        public String ImageHttpRequest(String requestURL,HashMap<String, String> PData) {

            StringBuilder stringBuilder = new StringBuilder();

            try {

                URL url;
                HttpURLConnection httpURLConnectionObject ;
                OutputStream OutPutStream;
                BufferedWriter bufferedWriterObject ;
                BufferedReader bufferedReaderObject ;
                int RC ;

                url = new URL(requestURL);

                httpURLConnectionObject = (HttpURLConnection) url.openConnection();

                httpURLConnectionObject.setReadTimeout(19000);

                httpURLConnectionObject.setConnectTimeout(19000);

                httpURLConnectionObject.setRequestMethod("POST");

                httpURLConnectionObject.setDoInput(true);

                httpURLConnectionObject.setDoOutput(true);

                OutPutStream = httpURLConnectionObject.getOutputStream();

                bufferedWriterObject = new BufferedWriter(

                        new OutputStreamWriter(OutPutStream, "UTF-8"));

                bufferedWriterObject.write(bufferedWriterDataFN(PData));

                bufferedWriterObject.flush();

                bufferedWriterObject.close();

                OutPutStream.close();

                RC = httpURLConnectionObject.getResponseCode();

                if (RC == HttpsURLConnection.HTTP_OK) {

                    bufferedReaderObject = new BufferedReader(new InputStreamReader(httpURLConnectionObject.getInputStream()));

                    stringBuilder = new StringBuilder();

                    String RC2;

                    while ((RC2 = bufferedReaderObject.readLine()) != null){

                        stringBuilder.append(RC2);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return stringBuilder.toString();
        }

        private String bufferedWriterDataFN(HashMap<String, String> HashMapParams) throws UnsupportedEncodingException {

            StringBuilder stringBuilderObject;

            stringBuilderObject = new StringBuilder();

            for (Map.Entry<String, String> KEY : HashMapParams.entrySet()) {

                if (check)

                    check = false;
                else
                    stringBuilderObject.append("&");

                stringBuilderObject.append(URLEncoder.encode(KEY.getKey(), "UTF-8"));

                stringBuilderObject.append("=");

                stringBuilderObject.append(URLEncoder.encode(KEY.getValue(), "UTF-8"));
            }

            return stringBuilderObject.toString();
        }

    }

    public class CustomOnItemSelectedListener implements AdapterView.OnItemSelectedListener {

        String firstItem = String.valueOf(spinner1.getSelectedItem());

        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
            if (firstItem.equals(String.valueOf(spinner1.getSelectedItem()))) {
                // ToDo when first item is selected
            } else {
                Toast.makeText(parent.getContext(),
                        "Kamu memilih : " + parent.getItemAtPosition(pos).toString(),
                        Toast.LENGTH_LONG).show();
                agama = parent.getItemAtPosition(pos).toString();
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> arg) {

        }

    }




    /**
     * Background Async Task untuk mengambil detail anggota
     * */
    public class AmbilDetailAnggota extends AsyncTask<String, String, String> {

        /**
         * Sebelum memulai background thread Tampilkan Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(EditAnggotaActivity.this);
            pDialog.setMessage("Menunggu detail anggota. Tunggu sebentar...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }
        protected String doInBackground(String... args) {

            //ambil intent data
            Intent in = getIntent();

            // ambil XML values dari intent sebelumnya
            nama = in.getStringExtra(KEY_NAMA);
            alamat = in.getStringExtra(KEY_ALAMAT);
            //if (in.getStringExtra(KEY_GENDER) == null) {
             //   jk = "";
            //} else {
                jk = in.getStringExtra(KEY_GENDER);
            //}
            //if (in.getStringExtra(KEY_BAHASA) == null) {
            //    bahasa = "";
            //} else {
                bahasa = in.getStringExtra(KEY_BAHASA);;
            //}
            agama = in.getStringExtra(KEY_AGAMA);
            tanggal = in.getStringExtra(KEY_TANGGAL);


            gambar = in.getStringExtra(KEY_GAMBAR);

            return null;
        }

        protected void onPostExecute(String args) {
            ImageView image = (ImageView) findViewById(R.id.imageView);
            new EditAnggotaActivity.LoadImagefromUrl( ).execute( image, gambar);

            tvDateResult.setText("Tanggal dipilih : "+tanggal);


            spinner1.setAdapter(adapter);
            if (agama != null) {
                int spinnerPosition = adapter.getPosition(agama);
                spinner1.setSelection(spinnerPosition);
            }


            // Displaying all values on the screen
            txtNama = (EditText) findViewById(R.id.inputNama);
            txtAlamat = (EditText) findViewById(R.id.inputAlamat);

           // if(jk == ""){
            //    gender.setChecked(false);
             //   gender2.setChecked(false);
            //}else
             if(jk.equals("Pria")){
                gender.setChecked(true);
            } else if(jk.equals("Wanita"))
            {
                gender2.setChecked(true);
            }

            //id.setChecked(true);

            //if(bahasa != "") {

                System.out.println(bahasa);
                String[] separated = bahasa.split(",");
                for (int i = 0; i < separated.length; i++) {
                    System.out.println(separated[i]);

                    if (separated[i].equals("Bahasa Indonesia")) {
                        System.out.println("id FOUND");
                        id.setChecked(true);
                    }

                    if (separated[i].trim().equals("Bahasa Inggris")) {
                        System.out.println("en FOUND");
                        en.setChecked(true);
                    }

                    if (separated[i].trim().equals( "Bahasa Jepang")) {
                        System.out.println("jp FOUND");
                        jp.setChecked(true);
                    }

                    if (separated[i].trim().equals("Bahasa Korea")) {
                        System.out.println("kr FOUND");
                        kr.setChecked(true);
                    }
                }
            //}
            txtNama.setText(nama);
            txtAlamat.setText(alamat);

            // hilangkan kotak dialog setelah mendapatkan detail
            pDialog.dismiss();
        }

    }

    /**
     * Background Async Task untuk update data anggota
     * */
    public class SimpanAnggotaDetail extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(EditAnggotaActivity.this);
            pDialog.setMessage("Update Data Anggota...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        protected String doInBackground(String... args) {

            //ambil intent data id anggota
            Intent in = getIntent();

            //ambil XML values dari intent sebelumnya
            idmem = in.getStringExtra(KEY_ID);

            // gambil data dari EditTexts
            nama = txtNama.getText().toString();
            alamat = txtAlamat.getText().toString();


            try {
                DefaultHttpClient client = new DefaultHttpClient();
                String postURL = "http://178.128.83.56/crudxmlphp/updateanggota.php";
                HttpPost post = new HttpPost(postURL);
                // buat Parameter
                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("idmem", idmem));
                params.add(new BasicNameValuePair("nama", nama));
                params.add(new BasicNameValuePair("alamat", alamat));
                UrlEncodedFormEntity ent = new UrlEncodedFormEntity(params,HTTP.UTF_8);
                post.setEntity(ent);
                HttpResponse responsePOST = client.execute(post);
                HttpEntity resEntity = responsePOST.getEntity();
                if (resEntity != null) {
                    Log.i("RESPONSE",EntityUtils.toString(resEntity));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            //jika selesai menyimpan/update kembali ke activity semuanggotaactivity
            Intent i = new Intent(getApplicationContext(), SemuaAnggotaActivity.class);
            // tutup semua activity sebelumnya
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            return null;
        }

        protected void onPostExecute(String args) {
            // hilangkan kotak dialog setelah mendapatkan detail
            pDialog.dismiss();

        }
    }

    /*****************************************************************
     * Background Async Task untuk menghapus anggota
     * */
    class HapusAnggota extends AsyncTask<String, String, String> {

        /**
         * Sebelum memulai background thread Tampilkan Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(EditAnggotaActivity.this);
            pDialog.setMessage("Hapus Data Anggota...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        protected String doInBackground(String... args) {

            //ambil intent data id anggota
            Intent in = getIntent();

            //ambil XML values dari intent sebelumnya
            idmem = in.getStringExtra(KEY_ID);

            try {
                DefaultHttpClient client = new DefaultHttpClient();
                String postURL = "http://178.128.83.56/crudxmlphp/hapusanggota.php";
                HttpPost post = new HttpPost(postURL);
                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("idmem", idmem));
                UrlEncodedFormEntity ent = new UrlEncodedFormEntity(params,HTTP.UTF_8);
                post.setEntity(ent);
                HttpResponse responsePOST = client.execute(post);
                HttpEntity resEntity = responsePOST.getEntity();
                if (resEntity != null) {
                    Log.i("RESPONSE",EntityUtils.toString(resEntity));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            //jika selesai menghapus kembali ke activity semuanggotaactivity
            Intent i = new Intent(getApplicationContext(), SemuaAnggotaActivity.class);
            startActivity(i);
            finish();
            return null;
        }

        protected void onPostExecute(String args) {
            // hilangkan kotak dialog setelah mendapatkan detail
            pDialog.dismiss();
        }

    }

}