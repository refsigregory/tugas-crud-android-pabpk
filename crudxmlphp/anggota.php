<?php
//koneksi database mysql
$dbhost = "localhost";
$dbuser	= "root";
$dbpass	= "";
$dbname	= "anggota1";
$conn = mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);

//query ambil data member
$sql = "SELECT * FROM member";
$q	 = mysqli_query($conn,$sql) or die(mysqli_error());
//buat elemen root
$xml = "<memberlist>";
//Ambil data dari database dan diletakkan ke dalam elemen root
while($r = mysqli_fetch_array($q)){
  $xml .= "<member>";
  $xml .= "<id>".$r['id']."</id>";  
  $xml .= "<nama>".$r['nama']."</nama>";
  $xml .= "<alamat>".$r['alamat']."</alamat>";
  $xml .= "<agama>".$r['agama']."</agama>";
  $xml .= "<gender>".$r['jenis_kelamin']."</gender>";
  $xml .= "<bahasa>".$r['bahasa']."</bahasa>";
  $xml .= "<tanggal>".$r['tanggal_lahir']."</tanggal>";
  $xml .= "<gambar>".$r['gambar']."</gambar>";
  $xml .= "</member>";  
}
$xml .= "</memberlist>";
//instansiasi objek SimpleXMLElement
$sxe = new SimpleXMLElement($xml);
$sxe->asXML("member.xml");
