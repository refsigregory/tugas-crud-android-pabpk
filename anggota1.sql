-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 30, 2018 at 08:41 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `anggota1`
--

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `id` tinyint(4) UNSIGNED NOT NULL,
  `nama` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `alamat` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `jenis_kelamin` enum('Pria','Wanita') COLLATE utf8_unicode_ci NOT NULL,
  `agama` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `tanggal_lahir` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `bahasa` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `gambar` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`id`, `nama`, `alamat`, `jenis_kelamin`, `agama`, `tanggal_lahir`, `bahasa`, `gambar`) VALUES
(1, 'Refsi Sangkay', 'Lahendong', 'Pria', 'Katolik', '16-11-1998', 'Bahasa Indonesia,Bahasa Inggris,Bahasa Korea,Bahasa Jepang,', 'http://178.128.83.56/crudxmlphp/images/23.png'),
(2, 'Al', '', 'Wanita', 'Kristen', '28-11-2018', 'Bahasa Indonesia,Bahasa Inggris,', 'http://178.128.83.56/crudxmlphp/images/23.png'),
(3, 'Michele', '', 'Wanita', 'Hindu', '13-11-2018', 'Bahasa Korea,Bahasa Korea,', 'http://178.128.83.56/crudxmlphp/images/11.png'),
(4, 'Tika', 'Perum', 'Wanita', 'Kristen', '19-11-2018', 'Bahasa Indonesia,Bahasa Inggris,', 'http://178.128.83.56/crudxmlphp/images/23.png'),
(5, 'Alo', 'Manado', 'Pria', 'Kristen', '', 'Bahasa Indonesia,', 'http://178.128.83.56/crudxmlphp/images/13.png');

-- --------------------------------------------------------

--
-- Table structure for table `uploadimagetoserver`
--

CREATE TABLE `uploadimagetoserver` (
  `id` int(11) NOT NULL,
  `image_path` varchar(100) NOT NULL,
  `image_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `uploadimagetoserver`
--

INSERT INTO `uploadimagetoserver` (`id`, `image_path`, `image_name`) VALUES
(1, 'http://178.128.83.56/crudxmlphp/images/0.png', 'a'),
(2, 'http://178.128.83.56/crudxmlphp/images/1.png', ''),
(3, 'http://178.128.83.56/crudxmlphp/images/2.png', 'a'),
(4, 'http://178.128.83.56/crudxmlphp/images/3.png', 'ah'),
(5, 'http://178.128.83.56/crudxmlphp/images/4.png', ''),
(6, 'http://178.128.83.56/crudxmlphp/images/5.png', ''),
(7, 'http://178.128.83.56/crudxmlphp/images/6.png', ''),
(8, 'http://178.128.83.56/crudxmlphp/images/7.png', ''),
(9, '', ''),
(10, 'http://178.128.83.56/crudxmlphp/images/9.png', ''),
(11, 'http://178.128.83.56/crudxmlphp/images/10.png', ''),
(12, 'http://178.128.83.56/crudxmlphp/images/11.png', ''),
(13, 'http://178.128.83.56/crudxmlphp/images/12.png', ''),
(14, 'http://178.128.83.56/crudxmlphp/images/13.png', ''),
(15, 'http://178.128.83.56/crudxmlphp/images/14.png', ''),
(16, 'http://178.128.83.56/crudxmlphp/images/15.png', ''),
(17, 'http://178.128.83.56/crudxmlphp/images/16.png', ''),
(18, 'http://178.128.83.56/crudxmlphp/images/17.png', ''),
(19, 'http://178.128.83.56/crudxmlphp/images/18.png', ''),
(20, 'http://178.128.83.56/crudxmlphp/images/19.png', ''),
(21, 'http://178.128.83.56/crudxmlphp/images/20.png', ''),
(22, 'http://178.128.83.56/crudxmlphp/images/21.png', ''),
(23, 'http://178.128.83.56/crudxmlphp/images/22.png', ''),
(24, 'http://178.128.83.56/crudxmlphp/images/23.png', ''),
(25, 'http://178.128.83.56/crudxmlphp/images/24.png', ''),
(26, 'http://178.128.83.56/crudxmlphp/images/25.png', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `uploadimagetoserver`
--
ALTER TABLE `uploadimagetoserver`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `id` tinyint(4) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `uploadimagetoserver`
--
ALTER TABLE `uploadimagetoserver`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
